package by.coaching.coach.dto;

import by.coaching.coach.entity.Role;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Data
public class AuthentificationDto {
    private Long id;
    @NotBlank(message = "Логин должен быть не пустым и не состоять из одних пробелов.")
    private String login;
}
