package by.coaching.coach.dto;

import lombok.Data;
import net.minidev.json.annotate.JsonIgnore;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PositiveOrZero;

@Data
public class WorkoutDto {
    private Long id;
    @NotBlank(message = "Дата должна быть заполнена.")
    private String date;
    @NotEmpty(message = "Выберите упражнение.")
    private Long exerciseId;
    @JsonIgnore
    private String exerciseName;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 999, message = "Значение должно быть не более 999.99.")
    private double calories;
    private String note;
    private StrengthDto strength;
    private EnduranceDto endurance;
    @NotEmpty(message = "Выберите имя клиента.")
    private Long clientId;
    @JsonIgnore
    private String clientName;
}
