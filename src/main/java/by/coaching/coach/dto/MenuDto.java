package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class MenuDto {
    private Long id;
    private String date;
    private List<DishDto> dishList;
}
