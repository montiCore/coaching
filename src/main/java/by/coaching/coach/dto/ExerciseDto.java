package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ExerciseDto {
    private Long id;
    @NotBlank(message = "Название должно быть не пустым и не состоять из одних пробелов.")
    private String name;
    private String description;
}
