package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;

@Data
public class StrengthDto {
    private Long id;
    @Positive(message = "Значение должно быть более нуля.")
    @Max(value = 99, message = "Значение должно быть не более 99.")
    private int approach;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 9999, message = "Значение должно быть не более 9999.")
    private int count;
}
