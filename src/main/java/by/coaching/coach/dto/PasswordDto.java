package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class PasswordDto {
    @Size(min = 8, max = 32, message = "Пароль должен содержать от 8 до 32 символов.")
    @NotBlank(message = "Поле не должно быть пустым.")
    private String password;
    @NotBlank(message = "Поле не должно быть пустым.")
    private String repeat;
}
