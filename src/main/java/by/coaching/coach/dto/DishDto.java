package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;

@Data
public class DishDto {
    private Long id;
    @NotBlank
    private String name;
    private String description;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 999, message = "Значение должно быть не более 999.99.")
    private int proteins;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 999, message = "Значение должно быть не более 999.99.")
    private int fats;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 999, message = "Значение должно быть не более 999.99.")
    private int carbohydrates;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 999, message = "Значение должно быть не более 999.99.")
    private int calories;
}
