package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class ContactDto {
    private Long id;
    @Email
    private String email;
    private String telephone;
    private String skype;
    private String viber;
    private String telegram;
    private String whatsapp;
    private String instagram;
    private String vk;
    private String facebook;
}
