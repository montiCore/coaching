package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.PositiveOrZero;

@Data
public class EnduranceDto {
    private Long id;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 9999, message = "Значение должно быть не более 9999.99.")
    private double distance;
    @PositiveOrZero(message = "Значение должно быть более или равно нулю.")
    @Max(value = 9999, message = "Значение должно быть не более 9999.99.")
    private double time;
    private String unit;
}
