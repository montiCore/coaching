package by.coaching.coach.dto;

import lombok.Data;

@Data
public class MenuStatisticDto {
    private Long id;
    private String date;
    private Long clientId;
    private double calories;
}
