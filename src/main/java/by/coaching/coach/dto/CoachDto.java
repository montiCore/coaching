package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
public class CoachDto {
    private Long id;
    @NotBlank(message = "Имя должно быть не пустым и не состоять из одних пробелов.")
    private String name;
    private AuthentificationDto authentification;
    private ContactDto contact;
}
