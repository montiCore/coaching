package by.coaching.coach.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Data
public class MenuInfoDto {
    private Long id;
    @NotBlank(message = "Дата должна быть заполнена.")
    private String date;
    @NotEmpty(message = "Выберите блюдо.")
    private Long dishId;
}
