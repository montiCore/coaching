package by.coaching.coach.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "client")
public class Client extends Person {
    @ManyToMany(mappedBy = "clientList")
    private List<Coach> coachList;
    @OneToMany(mappedBy = "client")
    private List<Workout> workoutList;
    @OneToMany(mappedBy = "client", fetch = FetchType.LAZY)
    private List<Menu> menuList;
    @OneToMany(mappedBy = "client")
    private List<Dish> dishList;
}
