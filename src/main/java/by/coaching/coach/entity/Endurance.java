package by.coaching.coach.entity;

import by.coaching.coach.util.Round;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "endurance")
public class Endurance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "distance")
    private double distance;
    @Column(name = "time")
    private double time;
    @Column(name = "unit")
    private String unit;

    @PrePersist
    @PreUpdate
    private void roundUpDoubleValues() {
        this.distance = Round.roundUp(this.distance);
        this.time = Round.roundUp(this.time);
    }
}
