package by.coaching.coach.entity;

import by.coaching.coach.annotation.View;
import by.coaching.coach.util.Round;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@View
@Entity
@Table(name = "menu_statistic")
public class MenuStatistic {
    @Id
    @Column(name = "id")
    private Long id;

    @Column(name = "date")
    private Date date;

    @Column(name = "client_id")
    private String clientId;

    @Column(name = "calories_common")
    private double calories;

    @PostLoad
    @PostPersist
    @PostUpdate
    private void roundUp() {
        this.calories = Round.roundUp(this.calories);
    }
}
