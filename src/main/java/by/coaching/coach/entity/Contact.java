package by.coaching.coach.entity;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "email", unique = true)
    private String email;
    @Column(name = "telephone", unique = true)
    private String telephone;
    @Column(name = "skype", unique = true)
    private String skype;
    @Column(name = "viber")
    private String viber;
    @Column(name = "telegram")
    private String telegram;
    @Column(name = "whatsapp")
    private String whatsapp;
    @Column(name = "instagram")
    private String instagram;
    @Column(name = "vk", unique = true)
    private String vk;
    @Column(name = "facebook", unique = true)
    private String facebook;
}
