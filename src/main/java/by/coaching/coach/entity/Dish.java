package by.coaching.coach.entity;

import by.coaching.coach.util.Round;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "dish")
public class Dish extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "description", nullable = false)
    private String description;
    @Column(name = "proteins")
    private double proteins;
    @Column(name = "fats")
    private double fats;
    @Column(name = "carbohydrates")
    private double carbohydrates;
    @Column(name = "calories")
    private double calories;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;

    @PrePersist
    @PreUpdate
    private void roundUpDoubleValues() {
        this.proteins = Round.roundUp(this.proteins);
        this.fats = Round.roundUp(this.proteins);
        this.carbohydrates = Round.roundUp(this.proteins);
        this.calories = Round.roundUp(this.proteins);
    }
}
