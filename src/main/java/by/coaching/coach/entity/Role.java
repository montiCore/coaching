package by.coaching.coach.entity;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;

public enum Role {
    CLIENT, COACH;
}
