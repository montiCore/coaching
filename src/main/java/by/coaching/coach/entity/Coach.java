package by.coaching.coach.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "coach")
public class Coach extends Person {
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "client_list",
            joinColumns = @JoinColumn(name = "client_id"),
            inverseJoinColumns = @JoinColumn(name = "coach_id"))
    private List<Client> clientList;
    @OneToMany(mappedBy = "coach", cascade = CascadeType.ALL)
    private List<Workout> workoutList;
}
