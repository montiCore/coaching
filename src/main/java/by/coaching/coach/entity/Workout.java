package by.coaching.coach.entity;

import by.coaching.coach.util.Round;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "workout")
public class Workout extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    @Column(name = "date", nullable = false)
    private Date date;
    @OneToOne
    @JoinColumn(name = "exercise_id", nullable = false)
    private Exercise exercise;
    @Column(name = "calories")
    private double calories;
    @Column(name = "note")
    private String note;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "strength_id")
    private Strength strength;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "endurance_id")
    private Endurance endurance;
    @ManyToOne
    @JoinColumn(name = "coach_id", nullable = false)
    private Coach coach;
    @ManyToOne
    @JoinColumn(name = "client_id", nullable = false)
    private Client client;

    @PrePersist
    @PreUpdate
    private void roundUpDoubleValues() {
        this.calories = Round.roundUp(this.calories);
    }
}
