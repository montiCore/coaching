package by.coaching.coach.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "authentification")
public class Authentification extends BaseEntity<Long> {

    @Autowired
    private static BCryptPasswordEncoder bCryptPasswordEncoder;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "login", unique = true, nullable = false)
    private String login;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "active")
    private boolean active;
    @OneToOne(mappedBy = "authentification")
    private Coach coach;
    @OneToOne(mappedBy = "authentification")
    private Client client;
    @ElementCollection(targetClass = Role.class, fetch = FetchType.EAGER)
    @CollectionTable(name = "role", joinColumns = @JoinColumn(name = "user_id"))
    @Enumerated(EnumType.STRING)
    private Set<Role> roles;

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return roles.stream().map(role -> new SimpleGrantedAuthority(role.toString()))
                .collect(Collectors.toSet());
    }

    public static UserDetails fromUser(Authentification authentification) {
        return new User(authentification.getLogin(),
                authentification.getPassword(),
                authentification.isActive(),
                authentification.isActive(),
                authentification.isActive(),
                authentification.isActive(),
                authentification.getAuthorities());
    }
}
