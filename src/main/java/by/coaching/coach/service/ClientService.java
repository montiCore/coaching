package by.coaching.coach.service;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.entity.Authentification;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Role;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.ClientListMapper;
import by.coaching.coach.mapper.ClientMapper;
import by.coaching.coach.mapper.WorkoutListMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static by.coaching.coach.common.ErrorDescriptions.USER_NOT_FOUND;
import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Service
public class ClientService {
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ClientMapper clientMapper;
    @Autowired
    private ClientListMapper clientListMapper;
    @Autowired
    private AuthentificationRepository authentificationRepository;
    @Autowired
    private WorkoutListMapper workoutListMapper;

    public void addClient(ClientDto clientDto, PasswordDto passwordDto) {
        Client client = clientMapper.toEntity(clientDto);
        client.getAuthentification().setRoles(Collections.singleton(Role.COACH));
        client.getAuthentification().setActive(true);
        client.getAuthentification().setPassword(passwordEncoder.encode(passwordDto.getPassword()));
        clientRepository.save(client);
    }

    public List<ClientDto> findClients(ClientDto clientDto) {
        Client client = clientMapper.toEntity(clientDto);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", contains().ignoreCase());
        Example<Client> example = Example.of(client, matcher);
        return clientListMapper.toDto(clientRepository.findAll(example));
    }

    public List<ClientDto> findClients(String name) {
        Client client = new Client();
        client.setName(name);
        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("name", contains().ignoreCase());
        Example<Client> example = Example.of(client, matcher);
        return clientListMapper.toDto(clientRepository.findAll(example));
    }

    public ClientDto findByUser(UserDetails user) {
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        return clientMapper.toDto(auth.getClient());
    }

    public void removeByUser(UserDetails user) {
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        clientRepository.delete(auth.getClient());
    }

    public List<WorkoutDto> getAllWorkoutsByUser(UserDetails user) {
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        return workoutListMapper.toDto(auth.getClient().getWorkoutList());
    }

    public void updateClientByUser(UserDetails user, ClientDto clientDto) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        clientDto.setId(client.getId());
        Client updatedClient = clientMapper.toEntity(clientDto);
        updatedClient.getAuthentification().setPassword(client.getAuthentification().getPassword());
        updatedClient.getAuthentification().setActive(client.getAuthentification().isActive());
        clientRepository.save(updatedClient);
    }

    public Collection<ClientDto> getAll() {
        return clientListMapper.toDto(clientRepository.findAll());
    }
}
