package by.coaching.coach.service;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Dish;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.DishListMapper;
import by.coaching.coach.mapper.DishMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.ClientRepository;
import by.coaching.coach.repository.DishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

import static by.coaching.coach.common.ErrorDescriptions.DISH_ID_NOT_FOUND;
import static by.coaching.coach.common.ErrorDescriptions.USER_NOT_FOUND;

@Service
public class DishService {
    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private AuthentificationRepository authentificationRepository;
    @Autowired
    private DishListMapper dishListMapper;
    @Autowired
    private ClientRepository clientRepository;


    public List<DishDto> getAllDishesByUser(UserDetails user) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        return dishListMapper.toDto(client.getDishList());
    }

    public void save(DishDto dishDto, UserDetails user) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        Dish dish = dishMapper.toEntity(dishDto);
        dish.setClient(client);
        dishRepository.save(dish);
    }

    public DishDto findById(Long id) {
        Dish dish = dishRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(DISH_ID_NOT_FOUND, id)));
        return dishMapper.toDto(dish);
    }

    public void removeById(Long id) {
        dishRepository.deleteById(id);
    }

    public Collection<DishDto> getAll() {
        return dishListMapper.toDto(dishRepository.findAll());
    }

    public void save(UserDetails user, DishDto dishDto) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        Dish dish = dishMapper.toEntity(dishDto);
        dish.setClient(client);
        dishRepository.save(dish);
    }
}
