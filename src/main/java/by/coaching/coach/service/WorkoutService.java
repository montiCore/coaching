package by.coaching.coach.service;

import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Coach;
import by.coaching.coach.entity.Exercise;
import by.coaching.coach.entity.Workout;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.WorkoutListMapper;
import by.coaching.coach.mapper.WorkoutMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.ClientRepository;
import by.coaching.coach.repository.ExerciseRepository;
import by.coaching.coach.repository.WorkoutRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;

import static by.coaching.coach.common.ErrorDescriptions.*;

@Service
public class WorkoutService {

    @Autowired
    private WorkoutRepository workoutRepository;
    @Autowired
    private WorkoutMapper workoutMapper;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private AuthentificationRepository authentificationRepository;
    @Autowired
    private WorkoutListMapper workoutListMapper;

    public void save(WorkoutDto workoutDto) {
        workoutRepository.save(workoutMapper.toEntity(workoutDto));
    }

    public WorkoutDto findById(Long id) {
        return workoutMapper.toDto(workoutRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(WORKOUT_ID_NOT_FOUND, id))));
    }

    public void removeById(Long id) {
        workoutRepository.deleteById(id);
    }

    public void save(WorkoutDto workoutDto, UserDetails user) {
        Workout workout = workoutMapper.toEntity(workoutDto);
        Coach coach = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getCoach();
        Client client = clientRepository.findById(workoutDto.getClientId())
                .orElseThrow(() -> new NotFoundException(String.format(CLIENT_ID_NOT_FOUND, workoutDto.getClientId())));
        Exercise exercise = exerciseRepository.findById(workoutDto.getExerciseId())
                .orElseThrow(() -> new NotFoundException(String.format(EXERCISE_ID_NOT_FOUND, workoutDto.getExerciseId())));
        workout.setCoach(coach);
        workout.setClient(client);
        workout.setExercise(exercise);
        workoutRepository.save(workout);
    }

    public Collection<WorkoutDto> getAll() {
        return workoutListMapper.toDto(workoutRepository.findAll());
    }
}
