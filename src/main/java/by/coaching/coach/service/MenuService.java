package by.coaching.coach.service;

import by.coaching.coach.dto.MenuDto;
import by.coaching.coach.dto.MenuInfoDto;
import by.coaching.coach.dto.MenuStatisticDto;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Dish;
import by.coaching.coach.entity.Menu;
import by.coaching.coach.entity.MenuStatistic;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.MenuListMapper;
import by.coaching.coach.mapper.MenuMapper;
import by.coaching.coach.mapper.MenuStatisticListenerMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.DishRepository;
import by.coaching.coach.repository.MenuRepository;
import by.coaching.coach.repository.MenuStatisticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

import static by.coaching.coach.common.ErrorDescriptions.*;

@Service
public class MenuService {
    @Autowired
    private MenuRepository menuRepository;
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private MenuListMapper menuListMapper;
    @Autowired
    private AuthentificationRepository authentificationRepository;
    @Autowired
    private DishRepository dishRepository;
    @Autowired
    private MenuStatisticRepository menuStatisticRepository;
    @Autowired
    private MenuStatisticListenerMapper menuStatisticListenerMapper;

    public List<MenuDto> getMenusByUser(User user) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        return menuListMapper.toDto(client.getMenuList());
    }


    public void save(MenuInfoDto menuInfoDto, UserDetails user) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        Dish dish = dishRepository.findById(menuInfoDto.getDishId())
                .orElseThrow(() -> new NotFoundException(String.format(DISH_ID_NOT_FOUND, menuInfoDto.getDishId())));
        Menu menu = menuRepository.findByDate(menuInfoDto.getDate()).orElse(new Menu());
        menu.setDate(Date.valueOf(menuInfoDto.getDate()));
        menu.getDishList().add(dish);
        menu.setClient(client);
        menuRepository.save(menu);
    }

    public void removeById(Long id) {
        menuRepository.deleteById(id);
    }

    public void removeDishById(Long menuId, Long dishId) {
        Menu menu = menuRepository.findById(menuId).orElseThrow(() -> new NotFoundException(String.format(MENU_ID_NOT_FOUND, menuId)));
        Dish dish = dishRepository.findById(dishId).orElseThrow(() -> new NotFoundException(String.format(DISH_ID_NOT_FOUND, dishId)));
        menu.getDishList().remove(dish);
        menuRepository.save(menu);
    }

    public List<MenuStatisticDto> getStatisticByUser(UserDetails user) {
        Client client = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getClient();
        List<MenuStatistic> menuStatisticList = menuStatisticRepository.findByClientId(client.getId());
        return menuStatisticListenerMapper.toDto(menuStatisticList);
    }

    public MenuDto findById(Long id) {
        Menu menu = menuRepository.findById(id).orElseThrow(() -> new NotFoundException(String.format(MENU_ID_NOT_FOUND, id)));
        return menuMapper.toDto(menu);
    }

    public Collection<MenuDto> getAll() {
        return menuListMapper.toDto(menuRepository.findAll());
    }
}
