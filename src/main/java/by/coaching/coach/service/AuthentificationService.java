package by.coaching.coach.service;

import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.entity.Authentification;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.exception.PasswordEqualException;
import by.coaching.coach.repository.AuthentificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static by.coaching.coach.common.ErrorDescriptions.USER_NOT_FOUND;

@RequiredArgsConstructor
@Service
public class AuthentificationService {
    private final AuthentificationRepository authentificationRepository;
    private final PasswordEncoder passwordEncoder;

    public boolean verifyLogin(String login) {
        Optional<Authentification> authOptional = authentificationRepository.findByLogin(login);
        return authOptional.isPresent();
    }

    public void updatePassword(UserDetails user, PasswordDto passwordDto) {
        if (!passwordDto.getPassword().equals(passwordDto.getRepeat())) {
            throw new PasswordEqualException("Введенные пароли не совпадают.");
        }
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        auth.setPassword(passwordEncoder.encode(passwordDto.getPassword()));
        authentificationRepository.save(auth);
    }
}
