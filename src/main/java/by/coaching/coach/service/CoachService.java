package by.coaching.coach.service;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.entity.Authentification;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Coach;
import by.coaching.coach.entity.Role;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.ClientListMapper;
import by.coaching.coach.mapper.CoachListMapper;
import by.coaching.coach.mapper.CoachMapper;
import by.coaching.coach.mapper.WorkoutListMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.ClientRepository;
import by.coaching.coach.repository.CoachRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static by.coaching.coach.common.ErrorDescriptions.CLIENT_ID_NOT_FOUND;
import static by.coaching.coach.common.ErrorDescriptions.USER_NOT_FOUND;

@RequiredArgsConstructor
@Service
public class CoachService {
    private final CoachRepository coachRepository;
    private final AuthentificationRepository authentificationRepository;
    private final CoachMapper coachMapper;
    private final ClientRepository clientRepository;
    private final ClientListMapper clientListMapper;
    private final PasswordEncoder passwordEncoder;
    private final WorkoutListMapper workoutListMapper;
    private final CoachListMapper coachListMapper;

    public void add(CoachDto coachDto, PasswordDto passwordDto) {
        Coach coach = coachMapper.toEntity(coachDto);
        coach.getAuthentification().setRoles(Collections.singleton(Role.COACH));
        coach.getAuthentification().setActive(true);
        coach.getAuthentification().setPassword(passwordEncoder.encode(passwordDto.getPassword()));
        coachRepository.save(coach);
    }

    public void updateCoachByUser(UserDetails user, CoachDto coachDto) {
        Coach coach = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getCoach();
        coachDto.setId(coach.getId());
        coachRepository.save(coachMapper.toEntity(coachDto));
    }

    public void removeByUser(UserDetails user) {
        Authentification authentification = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        coachRepository.delete(authentification.getCoach());
    }

    public CoachDto findByUser(UserDetails user) {
        Authentification authentification = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        return coachMapper.toDto(authentification.getCoach());
    }

    public List<ClientDto> getClientsByUser(UserDetails user) {
        Authentification authentification = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        return clientListMapper.toDto(authentification.getCoach().getClientList());
    }

    public void addClientToCoachById(Long clientId, UserDetails user) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new NotFoundException(String.format(CLIENT_ID_NOT_FOUND, clientId)));
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        Coach coach = auth.getCoach();
        coach.getClientList().add(client);
        coachRepository.save(coach);
    }

    public List<WorkoutDto> getAllWorkoutsByUser(UserDetails user) {
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        return workoutListMapper.toDto(auth.getCoach().getWorkoutList());
    }

    public void removeClientFromCoachById(Long clientId, UserDetails user) {
        Client client = clientRepository.findById(clientId)
                .orElseThrow(() -> new NotFoundException(String.format(CLIENT_ID_NOT_FOUND, clientId)));
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        Coach coach = auth.getCoach();
        coach.getClientList().remove(client);
        coachRepository.save(coach);
    }

    public Collection<CoachDto> getAll() {
        return coachListMapper.toDto(coachRepository.findAll());
    }
}
