package by.coaching.coach.service;

import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.entity.Authentification;
import by.coaching.coach.entity.Coach;
import by.coaching.coach.entity.Exercise;
import by.coaching.coach.exception.NotFoundException;
import by.coaching.coach.mapper.ExerciseListMapper;
import by.coaching.coach.mapper.ExerciseMapper;
import by.coaching.coach.repository.AuthentificationRepository;
import by.coaching.coach.repository.CoachRepository;
import by.coaching.coach.repository.ExerciseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

import static by.coaching.coach.common.ErrorDescriptions.EXERCISE_ID_NOT_FOUND;
import static by.coaching.coach.common.ErrorDescriptions.USER_NOT_FOUND;

@Service
public class ExerciseService {
    @Autowired
    private ExerciseRepository exerciseRepository;
    @Autowired
    private ExerciseMapper exerciseMapper;
    @Autowired
    private ExerciseListMapper exerciseListMapper;
    @Autowired
    private AuthentificationRepository authentificationRepository;
    @Autowired
    private CoachRepository coachRepository;

    public void save(ExerciseDto exerciseDto, UserDetails user) {
        Exercise exercise = exerciseMapper.toEntity(exerciseDto);
        Coach coach = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())))
                .getCoach();
        exercise.setCoach(coach);
        exerciseRepository.save(exercise);
    }

    public ExerciseDto findById(Long id) {
        Exercise exercise = exerciseRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(EXERCISE_ID_NOT_FOUND, id)));
        return exerciseMapper.toDto(exercise);
    }

    public void removeById(Long id) {
        exerciseRepository.deleteById(id);
    }

    public List<ExerciseDto> getAllByUser(UserDetails user) {
        Authentification auth = authentificationRepository.findByLogin(user.getUsername())
                .orElseThrow(() -> new NotFoundException(String.format(USER_NOT_FOUND, user.getUsername())));
        List<Exercise> exercises = exerciseRepository.getAllByCoachId(auth.getCoach().getId());
        return exerciseListMapper.toDto(exercises);
    }

    public Collection<ExerciseDto> getAll() {
        return exerciseListMapper.toDto(exerciseRepository.findAll());
    }
}

