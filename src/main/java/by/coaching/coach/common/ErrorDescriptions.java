package by.coaching.coach.common;

public class ErrorDescriptions {
    public static final String USER_NOT_FOUND = "Пользователь %s не найден в базе данных.";
    public static final String CLIENT_ID_NOT_FOUND = "Клиент с ID %s не найден в базе данных.";
    public static final String DISH_ID_NOT_FOUND = "Блюдо с ID %s не найдено в базе данных.";
    public static final String EXERCISE_ID_NOT_FOUND = "Упражнение с ID %s не найдено в базе данных.";
    public static final String MENU_ID_NOT_FOUND = "Меню с ID %s не найдено в базе данных.";
    public static final String WORKOUT_ID_NOT_FOUND = "Тренировка с ID %s не найдена в базе данных.";
}
