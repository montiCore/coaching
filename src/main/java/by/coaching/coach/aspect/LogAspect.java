package by.coaching.coach.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
public class LogAspect {

    @After("execution(* by.coaching.coach.*.*.*(..))")
    public void logging(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        StringBuilder logString = new StringBuilder("Вызов метода: ");
        logString.append(methodSignature + ". ");
        logString.append("Входящие параметры: ");
        if (joinPoint.getArgs().length != 0) {
            for (int i = 0; i < joinPoint.getArgs().length; i++) {
                logString.append(methodSignature.getParameterNames()[i] + " = " + joinPoint.getArgs()[i] + ";");
            }
        } else {
            logString.append(" параметры не определены.");
        }
        log.info(logString.toString());
    }
}
