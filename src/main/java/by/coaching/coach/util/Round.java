package by.coaching.coach.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Round {
    public static double roundUp(double value) {
        return new BigDecimal(value).setScale(2, RoundingMode.HALF_UP).doubleValue();
    }
}
