package by.coaching.coach.mapper;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.entity.Dish;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface DishMapper {

    Dish toEntity(DishDto dishDto);

    DishDto toDto(Dish dish);
}
