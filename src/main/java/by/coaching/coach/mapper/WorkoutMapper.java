package by.coaching.coach.mapper;

import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.entity.Workout;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {ExerciseMapper.class, CoachMapper.class, ClientMapper.class,
        EnduranceMapper.class, StrengthMapper.class})
public interface WorkoutMapper {
    @Mapping(target = "date", dateFormat = "yyyy-MM-dd")
    Workout toEntity(WorkoutDto workoutDto);

    @Mapping(target = "clientId", source = "client.id")
    @Mapping(target = "exerciseId", source = "exercise.id")
    @Mapping(target = "date", dateFormat = "yyyy-MM-dd")
    @Mapping(target = "exerciseName", source = "exercise.name")
    @Mapping(target = "clientName", source = "client.name")
    WorkoutDto toDto(Workout workout);
}
