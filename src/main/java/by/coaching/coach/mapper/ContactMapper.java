package by.coaching.coach.mapper;

import by.coaching.coach.dto.ContactDto;
import by.coaching.coach.entity.Contact;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface ContactMapper {
    ContactMapper INSTANCE = Mappers.getMapper(ContactMapper.class);

    Contact toEntity(ContactDto contactDto);

    ContactDto toDto(Contact contact);
}
