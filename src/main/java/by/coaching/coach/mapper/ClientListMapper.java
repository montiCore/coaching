package by.coaching.coach.mapper;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.entity.Client;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface ClientListMapper {
    List<ClientDto> toDto(List<Client> client);

    List<Client> toEntity(List<ClientDto> clientDto);
}
