package by.coaching.coach.mapper;

import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.entity.Exercise;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {CoachMapper.class})
public interface ExerciseMapper {
    Exercise toEntity(ExerciseDto exerciseDto);

    ExerciseDto toDto(Exercise exercise);
}
