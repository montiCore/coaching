package by.coaching.coach.mapper;

import by.coaching.coach.dto.AuthentificationDto;
import by.coaching.coach.entity.Authentification;
import org.mapstruct.Mapper;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface AuthentificationMapper {

    Authentification toEntity(AuthentificationDto authentificationDto);

    AuthentificationDto toDTO(Authentification authentification);
}
