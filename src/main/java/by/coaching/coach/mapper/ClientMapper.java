package by.coaching.coach.mapper;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.entity.Client;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {AuthentificationMapper.class, ContactMapper.class})
public interface ClientMapper {
    Client toEntity(ClientDto clientDto);

    ClientDto toDto(Client client);
}
