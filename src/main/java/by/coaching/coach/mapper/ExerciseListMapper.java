package by.coaching.coach.mapper;

import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.entity.Exercise;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = {CoachMapper.class})
public interface ExerciseListMapper {
    List<Exercise> toEntity(List<ExerciseDto> exerciseDtoList);

    List<ExerciseDto> toDto(List<Exercise> exerciseList);
}