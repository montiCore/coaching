package by.coaching.coach.mapper;

import by.coaching.coach.dto.MenuDto;
import by.coaching.coach.entity.Menu;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = {DishMapper.class})
public interface MenuListMapper {
    List<Menu> toEntity(List<MenuDto> menuDtoList);

    List<MenuDto> toDto(List<Menu> menuList);
}