package by.coaching.coach.mapper;

import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.entity.Coach;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = {WorkoutMapper.class, AuthentificationMapper.class, ContactMapper.class})
public interface CoachListMapper {
    List<Coach> toEntity(List<CoachDto> coachDtoList);

    List<CoachDto> toDto(List<Coach> coachList);
}
