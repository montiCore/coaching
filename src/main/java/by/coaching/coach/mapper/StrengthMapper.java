package by.coaching.coach.mapper;

import by.coaching.coach.dto.StrengthDto;
import by.coaching.coach.entity.Strength;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface StrengthMapper {
    Strength toEntity(StrengthDto strengthDto);

    StrengthDto toDto(Strength strength);
}
