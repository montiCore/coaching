package by.coaching.coach.mapper;

import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.entity.Workout;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring", uses = {WorkoutMapper.class, ExerciseMapper.class, CoachMapper.class, ClientMapper.class,
        EnduranceMapper.class, StrengthMapper.class})
public interface WorkoutListMapper {
    List<Workout> toEntity(List<WorkoutDto> workoutDtoList);

    List<WorkoutDto> toDto(List<Workout> workoutList);
}
