package by.coaching.coach.mapper;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.entity.Dish;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface DishListMapper {
    List<Dish> toEntity(List<DishDto> dishDto);

    List<DishDto> toDto(List<Dish> dish);
}
