package by.coaching.coach.mapper;

import by.coaching.coach.dto.EnduranceDto;
import by.coaching.coach.entity.Endurance;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface EnduranceMapper {
    Endurance toEntity(EnduranceDto enduranceDto);

    EnduranceDto toDto(Endurance endurance);
}
