package by.coaching.coach.mapper;

import by.coaching.coach.dto.MenuStatisticDto;
import by.coaching.coach.entity.MenuStatistic;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring")
public interface MenuStatisticMapper {
    MenuStatistic toEntity(MenuStatisticDto menuStatisticDto);

    MenuStatisticDto toDto(MenuStatistic menuStatistic);
}