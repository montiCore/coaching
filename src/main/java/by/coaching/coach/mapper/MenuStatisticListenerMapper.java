package by.coaching.coach.mapper;

import by.coaching.coach.dto.MenuStatisticDto;
import by.coaching.coach.entity.MenuStatistic;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Mapper(componentModel = "spring")
public interface MenuStatisticListenerMapper {
    List<MenuStatistic> toEntity(List<MenuStatisticDto> menuStatisticDtoList);

    List<MenuStatisticDto> toDto(List<MenuStatistic> menuStatisticList);
}
