package by.coaching.coach.mapper;

import by.coaching.coach.dto.MenuDto;
import by.coaching.coach.entity.Menu;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {DishMapper.class})
public interface MenuMapper {
    Menu toEntity(MenuDto coachDto);

    MenuDto toDto(Menu menu);
}