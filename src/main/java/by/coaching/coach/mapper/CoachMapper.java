package by.coaching.coach.mapper;

import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.entity.Coach;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", uses = {AuthentificationMapper.class, ContactMapper.class})
public interface CoachMapper {
    Coach toEntity(CoachDto coachDto);

    CoachDto toDto(Coach coach);
}
