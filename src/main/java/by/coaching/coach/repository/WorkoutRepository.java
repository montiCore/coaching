package by.coaching.coach.repository;

import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Coach;
import by.coaching.coach.entity.Workout;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkoutRepository extends JpaRepository<Workout, Long> {
/*    Optional<Workout> findWorkoutById(Long id);

    List<Workout> findAllWorkoutsByClient(Client client);

    List<Workout> findAllWorkoutsByCoaches(Coach coach);*/
}
