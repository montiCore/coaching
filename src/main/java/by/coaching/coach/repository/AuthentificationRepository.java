package by.coaching.coach.repository;

import by.coaching.coach.entity.Authentification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthentificationRepository extends JpaRepository<Authentification, Long> {
    @Query("SELECT a FROM Authentification a WHERE a.login LIKE :login")
    Optional<Authentification> findByLogin(@Param("login") String login);
}
