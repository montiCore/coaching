package by.coaching.coach.repository;

import by.coaching.coach.entity.Authentification;
import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Coach;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

}
