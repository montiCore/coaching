package by.coaching.coach.repository;

import by.coaching.coach.entity.Dish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DishRepository extends JpaRepository<Dish, Long> {
/*    Optional<Dish> findDishByName(String name);*/
}
