package by.coaching.coach.repository;

import by.coaching.coach.entity.Exercise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {
    @Query(value = "SELECT * FROM exercise AS e WHERE e.coach_id=:id", nativeQuery = true)
    List<Exercise> getAllByCoachId(@Param("id") Long id);
}
