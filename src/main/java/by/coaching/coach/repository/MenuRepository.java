package by.coaching.coach.repository;

import by.coaching.coach.entity.Client;
import by.coaching.coach.entity.Coach;
import by.coaching.coach.entity.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {
    @Query(value = "SELECT * FROM menu AS m WHERE m.date=:date", nativeQuery = true)
    Optional<Menu> findByDate(@Param("date") String date);
}
