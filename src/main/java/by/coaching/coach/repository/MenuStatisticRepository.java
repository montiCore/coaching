package by.coaching.coach.repository;

import by.coaching.coach.entity.MenuStatistic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MenuStatisticRepository extends JpaRepository<MenuStatistic, Long> {
    @Query(value = "SELECT * FROM menu_statistic c WHERE c.client_id=:id", nativeQuery = true)
    List<MenuStatistic> findByClientId(@Param("id") Long id);
}
