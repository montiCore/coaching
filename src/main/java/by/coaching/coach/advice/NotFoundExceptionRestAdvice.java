package by.coaching.coach.advice;

import by.coaching.coach.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class NotFoundExceptionRestAdvice {
    @ExceptionHandler
    public ResponseEntity<ErrorData> handleNotFoundException(NotFoundException exception) {
        ErrorData data = new ErrorData();
        data.setError(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.NOT_FOUND);
    }
}
