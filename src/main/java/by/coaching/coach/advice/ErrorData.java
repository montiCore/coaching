package by.coaching.coach.advice;

import lombok.Data;

@Data
public class ErrorData {
    private String error;
}
