package by.coaching.coach.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class RuntimeExceptionRestAdvice {
    @ExceptionHandler
    public ResponseEntity<ErrorData> handleException(RuntimeException exception) {
        ErrorData data = new ErrorData();
        data.setError(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
