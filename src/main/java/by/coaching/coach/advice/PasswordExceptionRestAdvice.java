package by.coaching.coach.advice;

import by.coaching.coach.exception.PasswordEqualException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PasswordExceptionRestAdvice {
    @ExceptionHandler
    public ResponseEntity<ErrorData> handlePasswordException(PasswordEqualException exception) {
        ErrorData data = new ErrorData();
        data.setError(exception.getMessage());
        return new ResponseEntity<>(data, HttpStatus.BAD_REQUEST);
    }
}
