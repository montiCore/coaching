package by.coaching.coach.controller;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.service.ClientService;
import by.coaching.coach.service.CoachService;
import by.coaching.coach.service.ExerciseService;
import by.coaching.coach.service.WorkoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@Controller
@RequestMapping(path = "/workout")
public class WorkoutController {
    @Autowired
    private CoachService coachService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private WorkoutService workoutService;

    @Autowired
    private ExerciseService exerciseService;

    @RequestMapping(value = "")
    public String show(@AuthenticationPrincipal User user, Model model) {
        List<WorkoutDto> workoutDtoList = coachService.getAllWorkoutsByUser(user);
        Collection<GrantedAuthority> authorities = user.getAuthorities();
        model.addAttribute("workouts", workoutDtoList);
        return "workout/list";
    }

    @RequestMapping(value = "/add")
    public String add(@AuthenticationPrincipal User user, Model model) {
        WorkoutDto workout = new WorkoutDto();
        CoachDto coach = coachService.findByUser(user);
        List<ExerciseDto> exercises = exerciseService.getAllByUser(user);
        List<ClientDto> clients = coachService.getClientsByUser(user);
        model.addAttribute("workout", workout);
        model.addAttribute("exercises", exercises);
        model.addAttribute("clients", clients);
        return "workout/info";
    }

    @PostMapping(value = "/save")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("workout") WorkoutDto workoutDto,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors()) {
            return "workout/info";
        }
        workoutService.save(workoutDto, user);
        return "redirect:/workout";
    }

    @RequestMapping(value = "/{id}/edit")
    public String edit(@AuthenticationPrincipal User user, @PathVariable("id") Long id, Model model) {
        WorkoutDto workout = workoutService.findById(id);
        List<ExerciseDto> exercises = exerciseService.getAllByUser(user);
        List<ClientDto> clients = coachService.getClientsByUser(user);
        model.addAttribute("workout", workout);
        model.addAttribute("exercises", exercises);
        model.addAttribute("clients", clients);
        return "workout/info";
    }

    @RequestMapping(value = "/{id}/remove")
    public String remove(@PathVariable("id") Long id) {
        workoutService.removeById(id);
        return "redirect:/workout";
    }

    @RequestMapping(value = "/{id}/details")
    public String details(@PathVariable("id") Long id, Model model) {
        model.addAttribute("workout", workoutService.findById(id));
        return "workout/details";
    }
}
