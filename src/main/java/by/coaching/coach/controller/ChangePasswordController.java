package by.coaching.coach.controller;

import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.service.AuthentificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@RequiredArgsConstructor
@Controller
@RequestMapping(path = "/auth")
public class ChangePasswordController {
    private final AuthentificationService authentificationService;

    @RequestMapping("/change_password")
    public String changePassword() {
        return "password/info";
    }

    @PostMapping("/change_password")
    public String updatePassword(@AuthenticationPrincipal User user,
                                 @Valid @ModelAttribute("password") PasswordDto passwordDto,
                                 BindingResult bindingResult,
                                 Model model) {
        if (bindingResult.hasErrors()) {
            return "password/info";
        }
        if (passwordDto.getPassword().equals(passwordDto.getRepeat())) {
            model.addAttribute("message", "Пароли не совпадают.");
            return "password/info";
        }
        authentificationService.updatePassword(user, passwordDto);
        return "redirect:/";
    }
}
