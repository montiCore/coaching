package by.coaching.coach.controller;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    @RequestMapping(value = "")
    public String show(@AuthenticationPrincipal User user, Model model) {
        List<DishDto> dishes = dishService.getAllDishesByUser(user);
        model.addAttribute("dishes", dishes);
        return "dish/list";
    }

    @RequestMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("dish", new DishDto());
        return "dish/info";
    }

    @RequestMapping(value = "/save")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("dish") DishDto dishDto,
                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "dish/info";
        }
        dishService.save(dishDto, user);
        return "redirect:/dish";
    }

    @RequestMapping(value = "/{id}/edit")
    public String edit(@PathVariable("id") Long id, Model model) {
        model.addAttribute("dish", dishService.findById(id));
        return "dish/info";
    }

    @RequestMapping(value = "/{id}/remove")
    public String remove(@PathVariable("id") Long id) {
        dishService.removeById(id);
        return "redirect:/dish";
    }

    @RequestMapping(value = "/{id}/details")
    public String details(@PathVariable("id") Long id, Model model) {
        DishDto dish = dishService.findById(id);
        model.addAttribute("dish", dish);
        return "dish/details";
    }
}
