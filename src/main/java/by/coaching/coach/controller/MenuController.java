package by.coaching.coach.controller;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.dto.MenuDto;
import by.coaching.coach.dto.MenuInfoDto;
import by.coaching.coach.service.DishService;
import by.coaching.coach.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/menu")
public class MenuController {
    @Autowired
    private MenuService menuService;
    @Autowired
    private DishService dishService;

    @RequestMapping(value = "")
    public String show(@AuthenticationPrincipal User user, Model model) {
        List<MenuDto> menus = menuService.getMenusByUser(user);
        model.addAttribute("menus", menus);
        return "menu/list";
    }


    @RequestMapping(value = "/statistic")
    public String getStatistic(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("statistics", menuService.getStatisticByUser(user));
        return "menu/statistic";
    }

    @RequestMapping(value = "/add")
    public String add(@AuthenticationPrincipal User user, Model model) {
        List<DishDto> dishes = dishService.getAllDishesByUser(user);
        model.addAttribute("dishes", dishes);
        model.addAttribute("menuInfo", new MenuInfoDto());
        return "menu/info";
    }

    @RequestMapping(value = "/save")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("menu") MenuInfoDto menuInfoDto,
                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "menu/info";
        }
        menuService.save(menuInfoDto, user);
        return "redirect:/menu";
    }

    @RequestMapping(value = "/{menuId}/remove")
    public String removeMenu(@PathVariable("menuId") Long id) {
        menuService.removeById(id);
        return "redirect:/menu";
    }

    @RequestMapping(value = "/{menuId}/dish/{dishId}/remove")
    public String removeDish(@PathVariable("menuId") Long menuId,
                             @PathVariable("dishId") Long dishId) {
        menuService.removeDishById(menuId, dishId);
        return "redirect:/menu";
    }
}
