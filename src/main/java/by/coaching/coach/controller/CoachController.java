package by.coaching.coach.controller;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.service.AuthentificationService;
import by.coaching.coach.service.ClientService;
import by.coaching.coach.service.CoachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/coach")
public class CoachController {

    @Autowired
    private CoachService coachService;
    @Autowired
    private AuthentificationService authentificationService;
    @Autowired
    private ClientService clientService;


    @RequestMapping(value = "/clients/{id}/add")
    public String addClient(@AuthenticationPrincipal User user, @PathVariable(name = "id") Long clientId) {
        coachService.addClientToCoachById(clientId, user);
        return "redirect:/coach/clients";
    }

    @RequestMapping(value = "/clients/{id}/remove")
    public String removeClient(@AuthenticationPrincipal User user,
                               @PathVariable(name = "id") Long clientId) {
        coachService.removeClientFromCoachById(clientId, user);
        return "redirect:/coach/clients";
    }

    @RequestMapping(value = "/clients")
    public String search(@AuthenticationPrincipal User user, Model model) {
        List<ClientDto> clientDtoList = coachService.getClientsByUser(user);
        model.addAttribute("client", new ClientDto());
        model.addAttribute("clients", clientDtoList);
        return "coach/clients";
    }

    @RequestMapping(value = "/find")
    public String find(@ModelAttribute("client") ClientDto clientDto, Model model) {
        List<ClientDto> result = clientService.findClients(clientDto);
        if (result.isEmpty()) {
            model.addAttribute("message", String.format("Пользователей с именем %s не найдено.", clientDto.getName()));
            model.addAttribute("client", clientDto);
            return "coach/clients";
        }
        model.addAttribute("clients", result);
        return "coach/find";
    }

    @GetMapping(value = "/edit")
    public String edit(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("coach", coachService.findByUser(user));
        return "coach/info";
    }

    @PostMapping(value = "/edit")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("coach") CoachDto coachDto,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors()) {
            return "coach/info";
        }
        boolean isLoginExists = authentificationService.verifyLogin(coachDto.getAuthentification().getLogin());
        if (isLoginExists && !user.getUsername().equals(coachDto.getAuthentification().getLogin())) {
            model.addAttribute("message", "Пользователь с таким логином уже существует.");
            model.addAttribute("coach", coachDto);
            return "coach/info";
        }
        coachService.updateCoachByUser(user, coachDto);
        return "redirect:/coach/details";
    }

    @RequestMapping(value = "/remove")
    public String remove(@AuthenticationPrincipal User user) {
        coachService.removeByUser(user);
        return "redirect:/logout";
    }

    @RequestMapping(value = "/details")
    public String details(@AuthenticationPrincipal User user, Model model) {
        model.addAttribute("coach", coachService.findByUser(user));
        return "client/details";
    }
}
