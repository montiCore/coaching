package by.coaching.coach.controller;

import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.service.CoachService;
import by.coaching.coach.service.ExerciseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(path = "/exercise")
public class ExerciseController {
    @Autowired
    private CoachService coachService;
    @Autowired
    private ExerciseService exerciseService;

    @RequestMapping(value = "")
    public String show(@AuthenticationPrincipal User user, Model model) {
        List<ExerciseDto> exercises = exerciseService.getAllByUser(user);
        model.addAttribute("exercises", exercises);
        return "exercise/exercises";
    }

    @RequestMapping(value = "/add")
    public String add(Model model) {
        model.addAttribute("exercise", new ExerciseDto());
        return "exercise/info";
    }

    @PostMapping(value = "/save")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("exercise") ExerciseDto exerciseDto,
                       BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "exercise/info";
        }
        exerciseService.save(exerciseDto, user);
        return "redirect:/exercise";
    }

    @RequestMapping(value = "/{id}/edit")
    public String edit(@PathVariable("id") Long id, Model model) {
        ExerciseDto exercise = exerciseService.findById(id);
        model.addAttribute("exercise", exercise);
        return "exercise/info";
    }

    @RequestMapping(value = "/{id}/remove")
    public String remove(@PathVariable("id") Long id) {
        exerciseService.removeById(id);
        return "redirect:/exercise";
    }

    @RequestMapping(value = "/{id}/details")
    public String details(@PathVariable("id") Long id, Model model) {
        ExerciseDto exercise = exerciseService.findById(id);
        model.addAttribute("exercise", exercise);
        return "exercise/details";
    }
}
