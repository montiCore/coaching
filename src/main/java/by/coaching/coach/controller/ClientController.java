package by.coaching.coach.controller;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.service.AuthentificationService;
import by.coaching.coach.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/client")
public class ClientController {

    @Autowired
    private ClientService clientService;
    @Autowired
    private AuthentificationService authentificationService;

    @RequestMapping(value = "/details")
    public String details(@AuthenticationPrincipal User user, Model model) {
        ClientDto client = clientService.findByUser(user);
        model.addAttribute("client", client);
        return "client/details";
    }

    @RequestMapping(value = "/edit")
    public String edit(@Valid @AuthenticationPrincipal User user, Model model) {
        ClientDto client = clientService.findByUser(user);
        model.addAttribute("client", client);
        return "client/info";
    }

    @PostMapping(value = "/edit")
    public String save(@AuthenticationPrincipal User user,
                       @Valid @ModelAttribute("client") ClientDto clientDto,
                       BindingResult bindingResult,
                       Model model) {
        if (bindingResult.hasErrors()) {
            return "client/info";
        }
        boolean isLoginExists = authentificationService.verifyLogin(clientDto.getAuthentification().getLogin());
        if (isLoginExists && !user.getUsername().equals(clientDto.getAuthentification().getLogin())) {
            model.addAttribute("message", "Пользователь с таким логином уже существует.");
            model.addAttribute("client", clientDto);
            return "client/info";
        }
        clientService.updateClientByUser(user, clientDto);
        return "redirect:/client/details";
    }

    @RequestMapping(value = "/remove")
    public String remove(@AuthenticationPrincipal User user) {
        clientService.removeByUser(user);
        return "redirect:/logout";
    }
}
