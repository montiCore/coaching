package by.coaching.coach.controller;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.service.AuthentificationService;
import by.coaching.coach.service.ClientService;
import by.coaching.coach.service.CoachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/registration")
public class RegistrationController {
    @Autowired
    private CoachService coachService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private AuthentificationService authentificationService;

    @PostMapping(value = "/coach")
    public String saveCoach(@Valid @ModelAttribute("coach") CoachDto coachDto,
                            @Valid @ModelAttribute("password") PasswordDto passwordDto,
                            BindingResult bindingResult,
                            Model model) {
        boolean isLoginExists = authentificationService.verifyLogin(coachDto.getAuthentification().getLogin());
        if (bindingResult.hasErrors()) {
            return "registration/coach";
        }
        if (!passwordDto.getPassword().equals(passwordDto.getRepeat())) {
            model.addAttribute("message", "Пароли не совпадают.");
            return "registration/coach";
        }
        if (isLoginExists) {
            model.addAttribute("message", "Пользователь с таким логином уже существует.");
            return "registration/coach";
        }
        coachService.add(coachDto, passwordDto);
        return "redirect:/";
    }

    @GetMapping(value = "/coach")
    public String registrationCoach(Model model) {
        model.addAttribute("coach", new CoachDto());
        model.addAttribute("password", new PasswordDto());
        return "registration/coach";
    }

    @PostMapping(value = "/client")
    public String saveClient(@Valid @ModelAttribute("client") ClientDto clientDto,
                             @Valid @ModelAttribute("password") PasswordDto passwordDto,
                             BindingResult bindingResult,
                             Model model) {
        if (bindingResult.hasErrors()) {
            return "registration/client";
        }
        boolean isLoginExists = authentificationService.verifyLogin(clientDto.getAuthentification().getLogin());
        if (!passwordDto.getPassword().equals(passwordDto.getRepeat())) {
            model.addAttribute("message", "Passwords are not equal.");
            return "registration/client";
        }
        if (isLoginExists) {
            model.addAttribute("message", "User exists!");
            return "registration/client";
        }
        clientService.addClient(clientDto, passwordDto);
        return "redirect:/";
    }

    @GetMapping(value = "/client")
    public String registrationClient(Model model) {
        model.addAttribute("client", new CoachDto());
        model.addAttribute("password", new PasswordDto());
        return "registration/client";
    }
}
