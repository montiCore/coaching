package by.coaching.coach.exception;

public class PasswordEqualException extends RuntimeException {
    public PasswordEqualException(String message) {
        super(message);
    }
}
