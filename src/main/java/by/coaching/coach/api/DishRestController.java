package by.coaching.coach.api;

import by.coaching.coach.dto.DishDto;
import by.coaching.coach.service.DishService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/dish")
@RestController
public class DishRestController {
    private final DishService dishService;

    @GetMapping("/{id}")
    public DishDto get(@PathVariable("id") Long id) {
        return dishService.findById(id);
    }

    @GetMapping("/all")
    public Collection<DishDto> getAll() {
        return dishService.getAll();
    }

    @PostMapping("/new")
    public void add(@AuthenticationPrincipal User user, @RequestBody DishDto dishDto) {
        dishService.save(user, dishDto);
    }

    @PutMapping("/")
    public void update(@AuthenticationPrincipal User user, @RequestBody DishDto dishDto) {
        dishService.save(user, dishDto);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        dishService.removeById(id);
    }
}
