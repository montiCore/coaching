package by.coaching.coach.api;

import by.coaching.coach.dto.ExerciseDto;
import by.coaching.coach.service.ExerciseService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/exercise")
@RestController
public class ExerciseRestController {
    private final ExerciseService exerciseService;

    @GetMapping("/{id}")
    public ExerciseDto get(@PathVariable("id") Long id) {
        return exerciseService.findById(id);
    }

    @GetMapping("/all")
    public Collection<ExerciseDto> getAll() {
        return exerciseService.getAll();
    }

    @PostMapping("/new")
    public void add(@AuthenticationPrincipal User user, @RequestBody ExerciseDto exerciseDto) {
        exerciseService.save(exerciseDto, user);
    }

    @PutMapping("/")
    public void update(@AuthenticationPrincipal User user, @RequestBody ExerciseDto exerciseDto) {
        exerciseService.save(exerciseDto, user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        exerciseService.removeById(id);
    }
}
