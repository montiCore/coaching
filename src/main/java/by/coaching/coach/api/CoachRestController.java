package by.coaching.coach.api;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.CoachDto;
import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.service.ClientService;
import by.coaching.coach.service.CoachService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/coach")
@RestController
public class CoachRestController {
    private final CoachService coachService;
    private final ClientService clientService;

    @GetMapping("/all")
    public Collection<CoachDto> getAll() {
        return coachService.getAll();
    }

    @GetMapping("/clients")
    public Collection<ClientDto> getClients(@AuthenticationPrincipal User user) {
        return coachService.getClientsByUser(user);
    }

    @GetMapping("/clients/{id}/add")
    public void addClient(@AuthenticationPrincipal User user, @PathVariable("id") Long clientId) {
        coachService.addClientToCoachById(clientId, user);
    }

    @GetMapping("/clients/{id}/remove")
    public void removeClient(@AuthenticationPrincipal User user, @PathVariable("id") Long clientId) {
        coachService.removeClientFromCoachById(clientId, user);
    }

    @GetMapping("/")
    public CoachDto get(@AuthenticationPrincipal User user) {
        return coachService.findByUser(user);
    }

    @PutMapping("/")
    public void update(@AuthenticationPrincipal User user, @RequestBody CoachDto coachDto) {
        coachService.updateCoachByUser(user, coachDto);
    }

    @DeleteMapping("/")
    public void remove(@AuthenticationPrincipal User user) {
        coachService.removeByUser(user);
    }
}
