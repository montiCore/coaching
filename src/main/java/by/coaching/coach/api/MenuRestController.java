package by.coaching.coach.api;

import by.coaching.coach.dto.MenuDto;
import by.coaching.coach.dto.MenuInfoDto;
import by.coaching.coach.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/menu")
@RestController
public class MenuRestController {
    private final MenuService menuService;

    @GetMapping("/{id}")
    public MenuDto get(@PathVariable("id") Long id) {
        return menuService.findById(id);
    }

    @GetMapping("/all")
    public Collection<MenuDto> getAll() {
        return menuService.getAll();
    }

    @PostMapping("/add")
    public void add(@AuthenticationPrincipal User user, @RequestBody MenuInfoDto menuInfoDto) {
        menuService.save(menuInfoDto, user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        menuService.removeById(id);
    }

    @DeleteMapping("/{menuId}/dish/{dishId}")
    public void delete(@PathVariable("menuId") Long menuId,
                       @PathVariable("dishId") Long dishId) {
        menuService.removeDishById(menuId, dishId);
    }
}
