package by.coaching.coach.api;

import by.coaching.coach.dto.ClientDto;
import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.service.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/client")
@RestController
public class ClientRestController {
    private final ClientService clientService;

    @GetMapping("/all")
    public Collection<ClientDto> getAll() {
        return clientService.getAll();
    }

    @GetMapping("/")
    public ClientDto get(@AuthenticationPrincipal User user) {
        return clientService.findByUser(user);
    }

    @PutMapping("/")
    public void update(@AuthenticationPrincipal User user, @RequestBody ClientDto clientDto) {
        clientService.updateClientByUser(user, clientDto);
    }

    @GetMapping("search/by_name")
    public Collection<ClientDto> searchByName(@RequestParam String name) {
        return clientService.findClients(name);
    }

    @GetMapping("/find")
    public Collection<ClientDto> findClients(@RequestBody ClientDto clientDto) {
        return clientService.findClients(clientDto);
    }

    @DeleteMapping("/")
    public void remove(@AuthenticationPrincipal User user) {
        clientService.removeByUser(user);
    }
}
