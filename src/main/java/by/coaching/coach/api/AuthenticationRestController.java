package by.coaching.coach.api;

import by.coaching.coach.dto.PasswordDto;
import by.coaching.coach.service.AuthentificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping(path = "/api/auth")
@RestController
public class AuthenticationRestController {
    private final AuthentificationService authentificationService;

    @PutMapping("/change_password")
    public void updatePassword(@AuthenticationPrincipal User user, @RequestBody PasswordDto passwordDto) {
        authentificationService.updatePassword(user, passwordDto);
    }
}
