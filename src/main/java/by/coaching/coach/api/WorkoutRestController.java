package by.coaching.coach.api;

import by.coaching.coach.dto.WorkoutDto;
import by.coaching.coach.service.WorkoutService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RequiredArgsConstructor
@RequestMapping(path = "/api/workout")
@RestController
public class WorkoutRestController {
    private final WorkoutService workoutService;

    @GetMapping("/{id}")
    public WorkoutDto get(@PathVariable("id") Long id) {
        return workoutService.findById(id);
    }

    @GetMapping("/all")
    public Collection<WorkoutDto> getAll() {
        return workoutService.getAll();
    }

    @PostMapping("/add")
    public void add(@AuthenticationPrincipal User user, @RequestBody WorkoutDto workoutDto) {
        workoutService.save(workoutDto, user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        workoutService.removeById(id);
    }

    @PutMapping("/")
    public void update(@AuthenticationPrincipal User user, @RequestBody WorkoutDto workoutDto) {
        workoutService.save(workoutDto, user);
    }
}
